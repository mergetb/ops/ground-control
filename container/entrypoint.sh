#!/bin/bash

if [[ -z $WAN_INTERFACE ]]; then
    echo "must specify WAN_INTERFACE environment variable"
    exit 1
fi

if [[ -z $LISTEN_ADDRESS_V4 ]]; then
    echo "must specify LISTEN_ADDRESS_V4 environment variable"
    exit 1
fi

if [[ -z $INTERNAL_INTERFACE ]]; then
    echo "must specify INTERNAL_INTERFACE environment varaible"
    exit 1
fi

setup_complete=/var/ground-control/setup-complete

if [[ ! -f $setup_complete ]]; then
    /usr/bin/ground-control setup -n $WAN_INTERFACE
    touch $setup_complete
fi
/usr/bin/ground-control run -4 $LISTEN_ADDRESS_V4 -i $INTERNAL_INTERFACE -s
