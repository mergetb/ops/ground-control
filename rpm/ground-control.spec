Name:           ground-control
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary:        Ground control is a bare-metal provisioning service for MergeTB nodes and switches.

License:        ASL 2.0
URL:            https://gitlab.com/mergetb/ops/%{name}
Source0:        https://gitlab.com/mergetb/ops/%{name}/-/releases/%{version}/%{name}-%{version}.tar.gz
BuildRequires:  golang ymk systemd-rpm-macros
# ymk is a mergetb ecosystem specific build tool

# this undefine fixes expectation of debugger entities
%global debug_package %{nil}
%define _build_id_links none

%description
The ground-control system is an integrated piece of the MergeTB
 facility software, that is used to both bootstrap and to manage and
 update a MergeTB testbed facility. It serves as an internal DNS
 resolver, a CumulusLinux ZTP script server, and a PXE boot shim to
 install Fedora CoreOS and "ignition" configure nodes.

# -s == file is not zero size, ! -s == file should be zero size
%prep
mkdir -vp %{_sourcedir}
pushd %{_sourcedir}
[[ -s %{name}-%{version}.tar.gz ]] || curl -sO %{url}/-/archive/%{version}/%{name}-%{version}.tar.gz
popd
%autosetup

%build
# output the current directory & build if binary does not yet exist
# the ymk binary/artifact may already exist in the gitlab CI pipeline
echo "PWD and directory listing" && pwd && ls -lh
ymk tools.yml
[[ -e build/ground-control ]] || ymk build.yml
# TODO: Documentation
# TODO: Makefile(s)

%install
rm -rf $RPM_BUILD_ROOT
install -v -D -m 0755 -t %{buildroot}/%{_bindir}/ build/* 
install -v -D -m 0644 -t %{buildroot}/%{_unitdir}/ rpm/%{name}.service
install -v -d -m 0755                       %{buildroot}/%{_sysconfdir}/sysconfig/
install -v -T -m 0644 rpm/%{name}.sysconfig %{buildroot}/%{_sysconfdir}/sysconfig/%{name}


%files
%{_bindir}/*
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%license           LICENSE
%doc               README.md


%changelog
#TODO: automate change inclusion since last approved MR
