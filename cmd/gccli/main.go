package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	groundcontrol "gitlab.com/mergetb/ops/ground-control/pkg/api"

	"google.golang.org/grpc"
)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "gccli",
		Short: "interact with a ground control server",
		Args:  cobra.NoArgs,
	}

	// status

	status := &cobra.Command{
		Use:   "status [node1 node2 ... nodeN]",
		Short: "get node or switch status",
		Run: func(cmd *cobra.Command, args []string) {
			status(args)
		},
	}
	root.AddCommand(status)

	// config

	config := &cobra.Command{
		Use:   "configure",
		Short: "configure nodes and switches",
	}
	root.AddCommand(config)

	// node config

	nc := new(groundcontrol.NodeConfig)
	nodeconf := &cobra.Command{
		Use:   "node <name>",
		Short: "configure node options",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			nc.Name = args[0]

			f, err := cmd.LocalFlags().GetString("boot")
			if err != nil {
				fmt.Println("unable to get boot flag")
				os.Exit(1)

			}
			v, ok := groundcontrol.BootState_value[strings.ToUpper(f)]
			if !ok {
				fmt.Println("invalid boot state (must be disk or install)")
			}
			nc.Boot = groundcontrol.BootState(v)
			configureNode(nc)
		},
	}
	nodeconf.Flags().StringVarP(&nc.Mac, "mac", "m", "", "mac address")
	nodeconf.Flags().StringVarP(&nc.Netdev, "netdev", "n", "", "mgmt netdev")
	nodeconf.Flags().StringVarP(&nc.Ignition, "ignition", "i", "", "ignition script")
	nodeconf.Flags().StringVarP(&nc.Ipv4, "ipv4", "4", "", "ipv4 address")
	nodeconf.Flags().StringP("boot", "b", "disk", "boot state (disk or install)")
	config.AddCommand(nodeconf)

	// switch config
	sc := new(groundcontrol.SwitchConfig)
	switchconf := &cobra.Command{
		Use:   "switch <name>",
		Short: "configure switch options",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			sc.Name = args[0]
			configureSwitch(sc)
		},
	}
	switchconf.Flags().StringVarP(&sc.Mac, "mac", "m", "", "mac address")
	switchconf.Flags().StringVarP(&sc.Ztp, "ztp", "z", "", "ztp script")
	switchconf.Flags().StringVarP(&sc.Ipv4, "ipv4", "4", "", "ipv4 address")
	config.AddCommand(switchconf)

	root.Execute()

	// power

	power := &cobra.Command{
		Use:   "power",
		Short: "control node and switch poer",
	}
	root.AddCommand(power)

	off := &cobra.Command{
		Use:   "off [node1 node2 ... nodeN]",
		Short: "turn a node or switch off",
		Run: func(cmd *cobra.Command, args []string) {
			powerOff(args)
		},
	}
	power.AddCommand(off)

	on := &cobra.Command{
		Use:   "on [node1 node2 ... nodeN]",
		Short: "turn a node or switch on",
		Run: func(cmd *cobra.Command, args []string) {
			powerOn(args)
		},
	}
	power.AddCommand(on)

	cycle := &cobra.Command{
		Use:   "cycle [node1 node2 ... nodeN]",
		Short: "cycle a node or switch",
		Run: func(cmd *cobra.Command, args []string) {
			powerCycle(args)
		},
	}
	power.AddCommand(cycle)

}

func status(nodes []string) {

}

type Callback func(groundcontrol.GroundControlClient) error

func withGC(f Callback) error {
	c, err := grpc.Dial(fmt.Sprintf("localhost:%d", groundcontrol.GRPCEndpoint), grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("dial: %w", err)
	}
	defer c.Close()
	gc := groundcontrol.NewGroundControlClient(c)
	return f(gc)
}

func configureNode(nc *groundcontrol.NodeConfig) {

	err := withGC(func(gc groundcontrol.GroundControlClient) error {
		_, err := gc.Configure(context.TODO(), &groundcontrol.ConfigureRequest{Node: nc})
		return err
	})

	if err != nil {
		fmt.Printf("grpc node configure error: %s\n", err)
		os.Exit(1)
	}
}

func configureSwitch(nc *groundcontrol.SwitchConfig) {

}

func powerOff(nodes []string) {

}

func powerOn(nodes []string) {

}

func powerCycle(nodes []string) {

}
