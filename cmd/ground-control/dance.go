package main

import (
	"fmt"
	"net"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/pkg/dance"

	"gitlab.com/mergetb/ops/ground-control/pkg/ipv4"
	"gitlab.com/mergetb/ops/ground-control/pkg/storage"
)

// dance server
var ds *dance.Server

// dynamic dance entries
var dd *storage.DanceData

func runDance() error {

	subnet := cfg.Subnet
	if cfg.SubnetDynamic != "" {
		subnet = cfg.SubnetDynamic
	}

	err := ipv4.InitIpv4Counter(subnet)
	if err != nil {
		log.Fatal(err)
	}

	ds, err = initDanceServer()
	if err != nil {
		log.Fatal(err)
	}

	// never returns
	ds.Run()

	return nil

}

func initDanceServer() (*dance.Server, error) {

	var err error

	da := dance.Ipv4AsUint32(danceipv4)
	fw := dance.Ipv4AsUint32(net.ParseIP(cfg.ForwardDNS))

	dn := &facility.DanceNetwork{
		DanceAddrV4:      da,
		SubnetMask:       subnetmask,
		Domain:           cfg.Domain,
		Gateway:          da,
		Search:           []string{cfg.Domain},
		ServiceInterface: danceifx,
		NextServer:       da,
		TftpBootloader:   "snponly.efi",
		DhcpOptions: map[uint32][]byte{
			239: []byte("http://ground-control/ztp"),
		},
		BindDnsAddr: bindDNSAddr,
		ForwardDns:  fw,
		LeaseTime:   cfg.LeaseTime,
	}

	dd, err = storage.NewDanceData()
	if err != nil {
		return nil, err
	}

	// load existing entries from storage
	entries, err := dd.GetEntries()
	if err != nil {
		entries = make(map[uint64]*facility.DanceEntry)
	}

	for _, x := range cfg.Nodes {

		names := []string{fmt.Sprintf("%s.%s", x.Name, cfg.Domain)}
		for _, a := range x.Alias {
			names = append(names, fmt.Sprintf("%s.%s", a, cfg.Domain))
		}

		entries[x.Parsed.Mac] = &facility.DanceEntry{
			Ipv4:         x.Parsed.Ipv4,
			Names:        names,
			TftpFilename: filepath.Base(x.FirmwareFile()),
		}

		err := ipv4.UseIpv4(x.Ipv4)
		if err != nil {
			return nil, fmt.Errorf("use Ipv4: %v", err)
		}

	}

	for _, x := range cfg.Switches {

		names := []string{fmt.Sprintf("%s.%s", x.Name, cfg.Domain)}
		for _, a := range x.Alias {
			names = append(names, fmt.Sprintf("%s.%s", a, cfg.Domain))
		}

		entries[x.Parsed.Mac] = &facility.DanceEntry{
			Ipv4:  x.Parsed.Ipv4,
			Names: names,
		}

		err := ipv4.UseIpv4(x.Ipv4)
		if err != nil {
			return nil, fmt.Errorf("use Ipv4: %v", err)
		}
	}

	// mark self as used
	err = ipv4.UseIpv4(danceipv4.String())
	if err != nil {
		return nil, err
	}

	services := make(map[string][]*facility.DanceService)
	for _, x := range cfg.Services {
		fqsn := fmt.Sprintf("_%s._%s.%s.%s", x.Service, x.Proto, cfg.Cluster, cfg.Domain)
		services[fqsn] = append(services[fqsn], x)
	}

	s := dance.NewServer(dn)
	s.Alloc = &LeaseAllocator{Domain: cfg.Domain, LeaseTime: cfg.LeaseTime}
	s.Cache.UpdateEntries(entries)
	s.Cache.UpdateServices(services)
	s.Cache.SetName4("ground-control", danceipv4)
	s.Cache.SetName4(fmt.Sprintf("ground-control.%s", cfg.Domain), danceipv4)
	s.Cache.SetName4("onie-server", danceipv4)
	s.Cache.SetName4(fmt.Sprintf("onie-server.%s", cfg.Domain), danceipv4)

	if cfg.Dns != nil {
		for _, wc := range cfg.Dns.Wildcard {
			for _, addr := range wc.Ipv4 {
				ip := net.ParseIP(addr)
				if ip == nil {
					log.Warnf(
						"suffix %s has bad ip address %s, ignoring",
						wc.Suffix,
						addr,
					)
					continue
				}
				fqdn := fmt.Sprintf("%s.%s", wc.Suffix, cfg.Domain)
				log.Infof("ADDING SUFFIX %s -> %s", fqdn, ip)
				s.Cache.SetWild4(fqdn, ip)

				err := ipv4.UseIpv4(addr)
				if err != nil {
					return nil, fmt.Errorf("use Ipv4: %v", err)
				}
			}
		}
		for _, e := range cfg.Dns.Entries {
			for _, addr := range e.Ipv4 {
				ip := net.ParseIP(addr)
				if ip == nil {
					log.Warnf(
						"entry %s has bad ip address %s, ignoring",
						e.Name,
						addr,
					)
					continue
				}
				fqdn := fmt.Sprintf("%s.%s", e.Name, cfg.Domain)
				log.Infof("ADDING NAME %s -> %s", fqdn, ip)
				s.Cache.SetName4(fqdn, ip)

				err := ipv4.UseIpv4(addr)
				if err != nil {
					return nil, fmt.Errorf("use Ipv4: %v", err)
				}
			}
		}
	}

	return s, nil

}

func addDanceEntry(mac net.HardwareAddr, entry *facility.DanceEntry) error {
	// add to storage
	err := dd.AddEntry(mac, entry)
	if err != nil {
		return err
	}

	// update in memory cache
	e := map[uint64]*facility.DanceEntry{
		dance.MacAsUint64(mac): entry,
	}
	ds.Cache.UpdateEntries(e)

	return nil
}

func delDanceEntry(mac net.HardwareAddr, entry *facility.DanceEntry) error {
	// remove from storeage
	err := dd.RemoveEntry(mac)
	if err != nil {
		return err
	}

	// update in memory cache
	e := map[uint64]*facility.DanceEntry{
		dance.MacAsUint64(mac): entry,
	}
	ds.Cache.RemoveEntries(e)

	return nil
}
