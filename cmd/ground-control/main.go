package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/ops/ground-control/pkg/api"
)

var (
	key         string
	cert        string
	skipVerify  bool
	corsdom     string
	configFile  string
	danceaddrv4 string
	danceifx    string
	natifx      string
	bindDNSAddr bool
	bindhttp    string
	bindtftp    string

	danceipv4 net.IP

	fcosVersion string
	fcosStream  string
)

func main() {

	root := &cobra.Command{
		Use:   "ground-control",
		Short: "a base configuration server for switches and servers",
		Args:  cobra.NoArgs,
	}
	root.PersistentFlags().StringVarP(
		&configFile, "config", "f", "/etc/ground-control.yml", "config file")

	run := &cobra.Command{
		Use:   "run",
		Short: "run the ground control server",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			validateRunFlags()
			run()
		},
	}
	run.Flags().StringVarP(&key, "key", "k", "", "tls key to use")
	run.Flags().StringVarP(&cert, "cert", "c", "", "tls cert to use")
	run.Flags().BoolVarP(
		&skipVerify, "skip-verify", "s", false, "skip tls verification")
	run.Flags().StringVarP(&corsdom, "cors", "d", "", "cors allowed domain")
	run.Flags().StringVarP(
		&danceaddrv4, "dhcp-dns-addrv4", "4", "", "dhcp/dns listening address")
	run.Flags().StringVarP(
		&danceifx, "dhcp-ifx", "i", "", "dhcp listening interface")
	run.Flags().BoolVarP(
		&bindDNSAddr, "bind-dns", "b", false, "bind dns onto the listening address")
	run.Flags().StringVarP(
		&bindhttp, "bind-http", "p", ":80", "bind http onto address")
	run.Flags().StringVarP(
		&bindtftp, "bind-tftp", "t", ":69", "bind tftp onto address")
	root.AddCommand(run)

	fetch := &cobra.Command{
		Use:   "setup",
		Short: "setup this server to run ground control",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {

			err := loadConfig()
			if err != nil {
				log.Fatalf("read config: %v", err)
			}

			if natifx == "" {
				log.Fatalf("must specify nat interface")
			}

			err = setupNat(natifx)
			if err != nil {
				log.Fatalf("setup nat: %v", err)
			}

			err = fetch()
			if err != nil {
				log.Fatalf("fetch: %v", err)
			}

		},
	}
	fetch.Flags().StringVarP(
		&natifx, "nat-ifx", "n", "", "nat WAN interface")

	/*
	   fetch.Flags().StringVarP(&fcosVersion,
	       "coreos-version", "c", "33.20210314.3.0", "Fedora CoreOS version")
	*/

	// Current fedorka core os is broken
	// https://github.com/openshift/okd/issues/566#issuecomment-803272970
	fetch.Flags().StringVarP(&fcosVersion,
		"coreos-version", "c", "35.20220424.3.0", "Fedora CoreOS version")

	fetch.Flags().StringVarP(&fcosStream,
		"coreos-stream", "s", "stable", "Fedora CoreOS stream")

	root.AddCommand(fetch)

	err := root.Execute()
	if err != nil {
		log.Fatal(err)
	}

}

func validateRunFlags() {
	if danceaddrv4 == "" {
		log.Fatal("must specify a dhcp/dns address")
	}
	danceipv4 = net.ParseIP(danceaddrv4)
	if danceipv4 == nil {
		log.Fatalf("invalid dhcp/dns address %s", danceaddrv4)
	}

	if danceifx == "" {
		log.Fatal("must specify a dhcp interface")
	}
}

func run() {

	err := loadConfig()
	if err != nil {
		log.Fatalf("read config: %v", err)
	}

	err = setupNat(natifx)
	if err != nil {
		log.Fatalf("setup nat: %v", err)
	}

	go doLeaseExpiry()
	go runDance()
	go runGrpc()
	go runGrpcGw()
	go runTftp()
	runHttp()

}

func runGrpc() {

	log.Info("running ground-control grpc endpoint")

	var server *grpc.Server

	if useCreds() {

		creds, err := credentials.NewServerTLSFromFile(key, cert)
		if err != nil {
			log.Fatalf("failed to set up tls: %v", err)
		}

		server = grpc.NewServer(grpc.Creds(creds))

	} else {

		log.Infof("not using creds")

		server = grpc.NewServer()

	}

	groundcontrol.RegisterGroundControlServer(server, &gs{})

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", groundcontrol.GRPCEndpoint))
	if err != nil {
		log.Fatalf("failed to listen on :%d: %v", groundcontrol.GRPCEndpoint, err)
	}

	log.Infof("listening for grpc connections on :%d", groundcontrol.GRPCEndpoint)
	log.Fatal(server.Serve(l))

}

func runGrpcGw() {

	log.Info("running ground-control grpcgw endpoint")

	opts := []grpc.DialOption{}
	if skipVerify {
		log.Infof("skipping TLS verification")
		creds := credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})
		opts = append(opts, grpc.WithTransportCredentials(creds))
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	err := groundcontrol.RegisterGroundControlHandlerFromEndpoint(
		ctx,
		mux,
		fmt.Sprintf(":%d", groundcontrol.GRPCEndpoint),
		opts,
	)
	if err != nil {
		log.Fatalf("register http gateway: %v", err)
	}

	var handler http.Handler = mux

	if corsdom != "" {
		handler = cors.New(cors.Options{
			AllowedOrigins: []string{fmt.Sprintf("https://*.%s", corsdom)},
			AllowedMethods: []string{
				http.MethodHead,
				http.MethodGet,
				http.MethodPost,
				http.MethodPut,
				http.MethodPatch,
				http.MethodDelete,
			},
			AllowedHeaders:   []string{"*"},
			AllowCredentials: true,
		}).Handler(handler)
	}

	log.Infof("listening for http connections on :%d", groundcontrol.HTTPEndpoint)
	log.Fatal(
		http.ListenAndServe(fmt.Sprintf(":%d", groundcontrol.HTTPEndpoint), handler),
	)

}

func useCreds() bool {

	if key != "" && cert != "" {
		return true
	}

	if key != "" {
		log.Fatalf("must specify cert when key is present")
	}
	if cert != "" {
		log.Fatalf("must specify key when cert is present")
	}

	return false

}
