package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"gopkg.in/yaml.v3"

	"gitlab.com/mergetb/facility/mars/pkg/dance"
	"gitlab.com/mergetb/ops/ground-control/pkg/config"
	"gitlab.com/mergetb/ops/ground-control/pkg/ipv4"
)

var (
	cfg *config.Config

	// parsed parameters
	subnetmask uint32
)

func loadConfig() error {

	if cfg != nil {
		return nil
	}

	src, err := os.ReadFile(configFile)
	if err != nil {
		return fmt.Errorf("read config file %s: %v", configFile, err)
	}

	cfg = new(config.Config)
	err = yaml.Unmarshal(src, cfg)
	if err != nil {
		return fmt.Errorf("unmarshal config: %v", err)
	}

	err = validateConfig()
	if err != nil {
		return err
	}

	// TODO implement a config file watcher

	return nil

}

func validateConfig() error {

	_, ipn, err := net.ParseCIDR(cfg.Subnet)
	if err != nil {
		return fmt.Errorf("parse subnet %s: %v", cfg.Subnet, err)
	}

	addr := net.ParseIP(cfg.ForwardDNS)
	if addr == nil {
		log.Printf("invalid ipv4 %s for dns forwarding, defaulting to 8.8.8.8", cfg.ForwardDNS)
		cfg.ForwardDNS = "8.8.8.8"
	}

	ones, _ := ipn.Mask.Size()
	subnetmask = uint32(ones)

	if cfg.SubnetDynamic == "" {
		log.Printf("SubnetDynamic not in use; static and dynamic IP addresses will be allocated from %s", cfg.Subnet)
	} else {
		_, dipn, err := net.ParseCIDR(cfg.SubnetDynamic)
		if err != nil {
			return fmt.Errorf("parse subnet %s: %v", cfg.SubnetDynamic, err)
		}

		// ensure ipn contains dipn
		contains, err := ipv4.Contains(ipn, dipn)
		if err != nil {
			return fmt.Errorf("ipv4.Contains: %v", err)
		}
		if !contains {
			return fmt.Errorf("Subnet (%s) does not fully contain SubnetDynamic (%s)",
				cfg.Subnet,
				cfg.SubnetDynamic,
			)
		}
	}

	for _, x := range cfg.Nodes {

		addr := net.ParseIP(x.Ipv4)
		if addr == nil {
			return fmt.Errorf("invalid ipv4 %s for node %s", x.Ipv4, x.Name)
		}
		x.Parsed.Ipv4 = dance.Ipv4AsUint32(addr.To4())

		mac, err := net.ParseMAC(x.Mac)
		if err != nil {
			return fmt.Errorf("invalid mac %s for node %s: %v", x.Mac, x.Name, err)
		}
		x.Parsed.Mac = dance.MacAsUint64(mac)

		if x.Ignition != "" {
			file := x.NodeConfig.IgnitionFile()

			_, err = os.Stat(file)
			if err != nil {
				return fmt.Errorf(
					"could not stat ignition config %s, needed for node %s",
					file, x.Name,
				)
			}
		}

		// coreos installer wants the full path name (https://gitlab.com/mergetb/facility/install/-/issues/34)
		if !strings.HasPrefix(x.Installdisk, "/dev/") {
			x.Installdisk = "/dev/" + x.Installdisk
		}
	}

	for _, x := range cfg.Switches {

		addr := net.ParseIP(x.Ipv4)
		if addr == nil {
			return fmt.Errorf("invalid ipv4 %s for switch %s", x.Ipv4, x.Name)
		}
		x.Parsed.Ipv4 = dance.Ipv4AsUint32(addr.To4())

		mac, err := net.ParseMAC(x.Mac)
		if err != nil {
			return fmt.Errorf("invalid mac %s for switch %s: %v", x.Mac, x.Name, err)
		}
		x.Parsed.Mac = dance.MacAsUint64(mac)

		if x.Ztp == "" {
			return fmt.Errorf("switch %s has no ztp config", x.Name)
		}

		file := x.SwitchConfig.ZtpFile()

		_, err = os.Stat(file)
		if err != nil {
			return fmt.Errorf(
				"could not stat ztp file %s, needed for switch %s",
				file, x.Name,
			)
		}
	}

	if cfg.LeaseTime == 0 {
		cfg.LeaseTime = 3600*4 // 4 hours by default
	}

	return nil

}
