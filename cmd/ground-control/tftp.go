package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/pin/tftp"
)

func runTftp() {

	readHandler := func(file string, rf io.ReaderFrom) error {

		base := "/var/ground-control/tftp/"

		filename := path.Clean(path.Join(base, file))

		if !strings.HasPrefix(filename, base) {
			return fmt.Errorf("cannot open %s, not in tftp directory", filename)
		}

		log.Printf("OPEN: %s", filename)

		f, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer f.Close()

		_, err = rf.ReadFrom(f)
		if err != nil {
			return err
		}

		return nil

	}

	writeHandler := func(path string, wt io.WriterTo) error { return nil }

	server := tftp.NewServer(readHandler, writeHandler)

	// Manually create the UDP connection so we can print the address and port that was used
	addr, err := net.ResolveUDPAddr("udp", bindtftp)
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("starting tftp server on %s", conn.LocalAddr())

	server.Serve(conn)

}
