package main

import (
	"context"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
	"gitlab.com/mergetb/ops/ground-control/pkg/api"
	"gitlab.com/mergetb/ops/ground-control/pkg/ipv4"
)

type gs struct{}

func (s *gs) Status(
	ctx context.Context, rq *groundcontrol.StatusRequest,
) (*groundcontrol.StatusResponse, error) {

	log.Debug("Status request")

	return &groundcontrol.StatusResponse{}, nil

}

func (s *gs) Configure(
	ctx context.Context, rq *groundcontrol.ConfigureRequest,
) (*groundcontrol.ConfigureResponse, error) {

	log.Debug("Configure request")

	ncs := cfg.GetNodes(rq.Node.Name)
	if ncs == nil {
		return &groundcontrol.ConfigureResponse{}, fmt.Errorf("unable to find node")
	}

	for _, nc := range ncs {
		// for now we just set the boot state of the node
		nc.Boot = rq.Node.Boot
	}

	return &groundcontrol.ConfigureResponse{}, nil

}

func (s *gs) Power(
	ctx context.Context, rq *groundcontrol.PowerRequest,
) (*groundcontrol.PowerResponse, error) {

	log.Debug("Power request")

	return &groundcontrol.PowerResponse{}, nil

}

func (s *gs) AddDance4Entry(
	ctx context.Context, rq *groundcontrol.Dance4Request,
) (*groundcontrol.Dance4Response, error) {

	var addr string
	var err error

	log.Debug("AddDance4Entry request")

	addr = rq.Ipv4
	ipv4 := net.ParseIP(addr)
	if ipv4 == nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to parse addr %s", addr)
	}

	mac, err := net.ParseMAC(rq.Mac)
	if err != nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to parse mac %s: %v", rq.Mac, err)
	}

	err = addDanceEntry(mac, &facility.DanceEntry{
		Ipv4:  dance.Ipv4AsUint32(ipv4.To4()),
		Names: rq.Names,
	})
	if err != nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to add dance4 entry for mac %s: %v", rq.Mac, err)
	}

	log.Infof("added dance4 entry for (names:%v, ip:%s, mac %s)", rq.Names, addr, rq.Mac)

	return &groundcontrol.Dance4Response{
		Ipv4: addr,
	}, nil

}

func (s *gs) DelDance4Entry(
	ctx context.Context, rq *groundcontrol.Dance4Request,
) (*groundcontrol.Dance4Response, error) {

	var addr string
	var err error

	log.Debug("DelDance4Entry request")

	addr = rq.Ipv4
	ipv4 := net.ParseIP(addr)
	if ipv4 == nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to parse addr %s", addr)
	}

	mac, err := net.ParseMAC(rq.Mac)
	if err != nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to parse mac %s: %v", rq.Mac, err)
	}

	err = delDanceEntry(mac, &facility.DanceEntry{
		Ipv4:  dance.Ipv4AsUint32(ipv4.To4()),
		Names: rq.Names,
	})
	if err != nil {
		return &groundcontrol.Dance4Response{}, fmt.Errorf("failed to del dance4 entry for mac %s: %v", rq.Mac, err)
	}

	log.Infof("deleted dance4 entry for (names:%v, ip:%s, mac %s)", rq.Names, addr, rq.Mac)

	return &groundcontrol.Dance4Response{}, nil

}

func (s *gs) NewIP4(
	ctx context.Context, rq *groundcontrol.IP4Request,
) (*groundcontrol.IP4Response, error) {

	log.Debug("NewIP4 request")

	bits, err := ipv4.GetSubnetBits(cfg.Subnet)
	if err != nil {
		return &groundcontrol.IP4Response{}, fmt.Errorf("internal err %v", err)
	}

	addr, err := ipv4.GetFreeIpv4()
	if err != nil {
		return &groundcontrol.IP4Response{}, fmt.Errorf("get free ipv4: %v", err)
	}

	log.Infof("allocated ipv4 %s", addr)

	return &groundcontrol.IP4Response{
		Ipv4: addr,
		Bits: int32(bits),
	}, nil

}

func (s *gs) FreeIP4(
	ctx context.Context, rq *groundcontrol.IP4Request,
) (*groundcontrol.IP4Response, error) {

	log.Debug("FreeIP4 request")

	err := ipv4.FreeIpv4(rq.Ipv4)
	if err != nil {
		return &groundcontrol.IP4Response{}, fmt.Errorf("failed to free addr %s: %v", rq.Ipv4, err)
	}

	log.Infof("freed ipv4 %s", rq.Ipv4)

	return &groundcontrol.IP4Response{
		Ipv4: rq.Ipv4,
	}, nil

}
