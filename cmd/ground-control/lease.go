package main

import (
	"fmt"
	"net"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
	"gitlab.com/mergetb/ops/ground-control/pkg/ipv4"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// satisfies dance.Allocator
type LeaseAllocator struct {
	Domain    string
	LeaseTime uint32
}

func (a *LeaseAllocator) Lease(hostname string, hwaddr net.HardwareAddr) (net.IP, error) {
	ip, err := ipv4.GetFreeIpv4()
	if err != nil {
		return nil, err
	}

	netip := net.ParseIP(ip)

	// if the client hostname is not provided, use a generic name
	if hostname == "" {
		parts := strings.Split(ip, ".")
		hostname = fmt.Sprintf("dhcp-%s-%s-%s-%s.%s", parts[0], parts[1], parts[2], parts[3], a.Domain)
	}

	err = addDanceEntry(hwaddr, &facility.DanceEntry{
		Ipv4:    dance.Ipv4AsUint32(netip),
		Names:   []string{hostname},
		Expires: timestamppb.New(time.Now().Add(time.Second * time.Duration(a.LeaseTime))),
	})

	if err != nil {
		return nil, err
	}

	return netip, nil
}

// renew the dhcp lease
func (a *LeaseAllocator) Renew(ip net.IP, mac net.HardwareAddr) error {
	d := dd.GetEntry(mac)
	if d == nil {
		// not a leased ip
		return nil
	}

	// don't update when there is no expiration time set
	// this happens when the harbor sets up its dance entry
	if d.Expires.GetSeconds() == 0 {
		return nil
	}

	d.Expires = timestamppb.New(time.Now().Add(time.Second * time.Duration(a.LeaseTime)))

	// update and save to disk
	err := dd.AddEntry(mac, d)
	if err != nil {
		return fmt.Errorf("store dance data: %w", err)
	}

	return err
}

func doLeaseExpiry() {
	for {
		time.Sleep(time.Duration(1) * time.Hour) // run once per hour

		log.Info("checking for expired leases")

		tbl, err := dd.GetEntries()
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("get dance entries")
			continue
		}

		now := time.Now()

		type foo struct {
			mac   net.HardwareAddr
			entry *facility.DanceEntry
		}
		var xs []*foo

		for mac, e := range tbl {
			if e.Expires.GetSeconds() > 0 && now.After(e.Expires.AsTime()) {
				log.WithFields(log.Fields{"ip": dance.Uint32AsIpv4(e.Ipv4)}).Info("lease expired")
				xs = append(xs, &foo{dance.Uint64AsMac(mac), e})
			}
		}

		for _, x := range xs {
			err = delDanceEntry(x.mac, x.entry)
			if err != nil {
				log.WithFields(log.Fields{"err": err}).Error("delete dance entry")
			}
		}
	}
}
