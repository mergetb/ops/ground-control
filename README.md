Ground Control
==============

Ground control is a bare-metal provisioning service for nodes and switches.
Supported platforms include

- Fedora CoreOS nodes
- RedHat CoreOS nodes
- ONIE-based switching platforms with zero-touch provisioning (ZTP)
    - Cumulus Linux
    - SONiC

## Getting

Ground control is still in early stages and we do not yet have formal releases.
You can pick up a build from artifacts produced by our 
[CI process](https://gitlab.com/mergetb/ops/ground-control/-/pipelines) though.

We also produce containerized builds available from 
[this repository's registry](https://gitlab.com/mergetb/ops/ground-control/container_registry).

## Usage

There are two basic steps to running ground control.

1. Initial setup
2. Running the daemon

### Setup

Setup requires that you specify the WAN interface through the `-n` flag at a
minimum. The WAN interface is used to setup a masquerading NAT for IPv4 traffic.
For example:

```
ground-control setup -n eth0
```

See `ground-control setup -h` for more setup options.

### Configuration

The Ground Control config file is located at `/etc/ground-control.yml` by
default (overridable via `--config` flag). Here is an example config

```yaml
domain: mars.example.com
subnet: 10.0.0.0/24
nodes:
  - name: ifr                  # name of the device, will resolve via dns
    mac: 04:70:00:00:00:02     # mac address used to identify device
    netdev: ens1               # the management interface of the device
    ipv4: 10.0.0.10            # address to give to the device
    ignition: ifr.ign          # CoreOS ignition file used to setup the device
    installdisk: /dev/sda      # where to install CoreOS

switches:
  - name: infra                # name of the switch, will resolve via dns
    mac: 04:70:00:00:00:03     # mac address used to identify switch
    ipv4: 10.0.0.11            # address to give to the switch
    ztp: cumulus-ztp.sh        # zero-touch provisioning script to use
```

### Running

Running requires you provide an IPv4 listening address and an internal
interface. The IPv4 address is used for listening for DNS request traffic and
the interface for broadcast DHCP traffic.  For example:

```
ground-control run -4 192.168.1.1 -i eth1 -s
```

Unless you have provided a cert to ground control that the OS trusts (e.g. like
something from Let's Encrypt), you will also need to specify the `-s` flag.


## Building

Building requires the [ymk](https://gitlab.com/mergetb/devops/ymk) utility.

```
ymk build.yml
ymk container.yml
```

## Dependencies

- `nftables`
