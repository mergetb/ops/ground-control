package config

import (
	facility "gitlab.com/mergetb/api/facility/v1/go"
	groundcontrol "gitlab.com/mergetb/ops/ground-control/pkg/api"
)

type Config struct {
	Domain        string
	Cluster       string
	Subnet        string
	SubnetDynamic string
	ForwardDNS    string
	Nodes         []*NodeConfig
	Switches      []*SwitchConfig
	Services      []*facility.DanceService
	Dns           *DnsConfig
	StaticFiles   []StaticFile
	OnieURL       string
	LeaseTime     uint32
}

type StaticFile struct {
	SourceURL    string
	DestFilename string
}

type DnsConfig struct {
	Entries  []DnsEntry
	Wildcard []WildcardEntry
}

type DnsEntry struct {
	Name string
	Ipv4 []string
}

type WildcardEntry struct {
	Suffix string
	Ipv4   []string
}

type NodeConfig struct {
	*groundcontrol.NodeConfig `yaml:",inline"`
	Parsed                    ParsedConfig `yaml:"-"`
}

type ParsedConfig struct {
	Mac  uint64
	Ipv4 uint32
}

type SwitchConfig struct {
	*groundcontrol.SwitchConfig `yaml:",inline"`
	Parsed                      ParsedConfig `yaml:"-"`
}

func (c *Config) GetNodes(name string) []*NodeConfig {
	var ncs []*NodeConfig

	for _, nc := range c.Nodes {
		if nc.Name == name {
			ncs = append(ncs, nc)
		}
	}
	return ncs
}
