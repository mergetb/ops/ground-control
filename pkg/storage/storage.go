package storage

// Stores serialized dance entries and IPv4 allocations to local storage to
// allow dynamic configurations made via the API to survive ground-control
// restarts

const (
	storage_dir string = "/var/ground-control/run"
)
