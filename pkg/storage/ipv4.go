package storage

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"

	"inet.af/netaddr"
)

var (
	ipv4_storage_dir string = fmt.Sprintf("%s/ipv4", storage_dir)
	ipv4_file        string = fmt.Sprintf("%s/ips.json", ipv4_storage_dir)
)

type Ipv4Data struct {
	// public
	Subnet  string
	Builder *netaddr.IPSetBuilder
}

type storage struct {
	Subnet   string // human readable string showing the subnet being tracked
	Prefixes []netaddr.IPPrefix
}

func Ipv4File() string {
	return ipv4_file
}

func (d *Ipv4Data) Store() error {
	err := os.MkdirAll(ipv4_storage_dir, 0755)
	if err != nil {
		return err
	}

	ipset, err := d.Builder.IPSet()
	if err != nil {
		log.Warnf("IPSet has errors: %v", err)
	}

	s := &storage{
		Subnet:   d.Subnet,
		Prefixes: ipset.Prefixes(),
	}

	dat, err := json.Marshal(s)
	if err != nil {
		return fmt.Errorf("json marshal: %v", err)
	}

	err = ioutil.WriteFile(ipv4_file, []byte(dat), 0644)
	if err != nil {
		return fmt.Errorf("writefile %s: %v", ipv4_file, err)
	}

	return nil
}

func (d *Ipv4Data) Load() error {
	dat, err := ioutil.ReadFile(ipv4_file)
	if err != nil {
		return fmt.Errorf("readfile %s: %v", ipv4_file, err)
	}

	var s storage
	err = json.Unmarshal(dat, &s)
	if err != nil {
		return fmt.Errorf("json unmarshal: %v", err)
	}

	d.Builder = new(netaddr.IPSetBuilder)
	for _, p := range s.Prefixes {
		d.Builder.AddPrefix(p)
	}
	d.Subnet = s.Subnet

	return nil
}
