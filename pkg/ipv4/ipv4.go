package ipv4

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"inet.af/netaddr"

	"gitlab.com/mergetb/ops/ground-control/pkg/storage"
)

// TODO: manage concurrency between GC server and API server? Not really necessary
// at the moment as GC does not touch this data structure after initialization

// in memory representation of available IP addresses to allocate
var data *storage.Ipv4Data

// Return whether subnetA fully contains subnetB
func Contains(subnetA, subnetB *net.IPNet) (bool, error) {

	prefixA, ok := netaddr.FromStdIPNet(subnetA)
	if !ok {
		return false, fmt.Errorf("failed to parse subnet")
	}

	prefixB, ok := netaddr.FromStdIPNet(subnetB)
	if !ok {
		return false, fmt.Errorf("failed to parse subnet")
	}

	rangeA := prefixA.Range()
	rangeB := prefixB.Range()

	fromB := rangeB.From()
	toB := rangeB.To()

	return rangeA.Contains(fromB) && rangeA.Contains(toB), nil
}

func InitIpv4Counter(subnet string) error {
	data = new(storage.Ipv4Data)

	err := data.Load()
	if err == nil {

		log.Infof("checking %s and %s", subnet, data.Subnet)
		if subnet == data.Subnet {
			return nil
		}

		return fmt.Errorf("found data in storage, but subnet does not equal the one in gc config (%s != %s). Remove %s and restart",
			data.Subnet, subnet, storage.Ipv4File(),
		)
	}

	// nothing in storage; create now
	prefix, err := netaddr.ParseIPPrefix(subnet)
	if err != nil {
		return fmt.Errorf("parse IP prefix %s: %v", subnet, err)
	}

	data.Subnet = subnet
	data.Builder = new(netaddr.IPSetBuilder)
	data.Builder.AddPrefix(prefix)
	return data.Store()
}

func GetSubnetBits(subnet string) (uint8, error) {
	p, err := netaddr.ParseIPPrefix(subnet)
	if err != nil {
		return 0, err
	}

	return p.Bits(), nil
}

// get first IP from first free range
func GetFreeIpv4() (string, error) {
	addr, err := getFreeIpv4()
	if err != nil {
		return "", err
	}

	err = data.Store()
	if err != nil {
		return "", err
	}

	return addr, nil
}

// mark an address as in use
func UseIpv4(addr string) error {
	ip, err := netaddr.ParseIP(addr)
	if err != nil {
		return fmt.Errorf("parse IP %s: %v", addr, err)
	}

	// use if in the range; if not in the range, not an error
	has := containsIpv4(ip)
	if !has {
		return nil
	}

	data.Builder.Remove(ip)
	return data.Store()
}

// free an address back to the pool
func FreeIpv4(addr string) error {
	ip, err := netaddr.ParseIP(addr)
	if err != nil {
		return fmt.Errorf("parse IP %s: %v", addr, err)
	}

	err = freeIpv4(ip)
	if err != nil {
		return err
	}

	return data.Store()
}

func UseIpv4Subnet(subnet string) error {
	prefix, err := netaddr.ParseIPPrefix(subnet)
	if err != nil {
		return fmt.Errorf("parse IP prefix %s: %v", subnet, err)
	}

	log.Infof("removing ipv4 subnet %s from allocator", subnet)

	data.Builder.RemovePrefix(prefix)
	return data.Store()
}

func getFreeIpv4() (string, error) {
	ipset, err := data.Builder.IPSet()
	if err != nil {
		log.Warnf("IPSet has errors: %v", err)
	}

	ranges := ipset.Ranges()
	if len(ranges) == 0 {
		return "", fmt.Errorf("no IP addresses left")
	}

	for _, r := range ranges {
		from := r.From()
		to := r.To()
		ip := from
		for {
			if ip.IsZero() {
				log.Warnf("found Zero IP address?")
				break
			}

			if to.Less(ip) {
				// go to next range
				break
			}

			// check if 0 or 255 address
			i4 := ip.As4()
			if i4[3] != 0 && i4[3] != 255 {
				data.Builder.Remove(ip)
				return ip.String(), nil
			}

			ip = ip.Next()
		}
	}

	return "", fmt.Errorf("no IP addresses left")
}

func freeIpv4(ip netaddr.IP) error {
	ipset, err := data.Builder.IPSet()
	if err != nil {
		log.Warnf("IPSet has errors: %v", err)
	}

	ranges := ipset.Ranges()
	for _, r := range ranges {
		if r.Contains(ip) {
			return fmt.Errorf("IPSet already has addr %s", ip.String())
		}
	}

	data.Builder.Add(ip)
	return nil
}

func containsIpv4(ip netaddr.IP) bool {
	ipset, err := data.Builder.IPSet()
	if err != nil {
		log.Warnf("IPSet has errors: %v", err)
	}

	ranges := ipset.Ranges()
	for _, r := range ranges {
		if r.Contains(ip) {
			return true
		}
	}

	return false
}
